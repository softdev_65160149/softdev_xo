/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.labxo;

/**
 *
 * @author chano
 */
import java.util.Scanner;
public class LABXo {
    private char[][] gboard;
    private char player;
    private boolean endgame;
    private char Continue;
    private boolean playcon; 
    private int pround = 1;
    
    public LABXo(){
        gboard= new char[3][3];
        player = 'X';
        endgame = false;

        for(int row = 0; row <3; row++){
            for(int col = 0; col<3; col++){
                gboard[row][col] ='-';
            }
        }
    }
    
    public void playOX(){
        System.out.println("Welcome to OX");
        playcon = true;
        while(playcon){
            resetboard();
            resetround();
            endgame =false;            
            while(!endgame){
                printBoard();
                System.out.println("Round"+pround);
                playerplay();
                gamestatus();
                switchplay();
            
        }
        System.out.println("Result");
        printBoard();
        Scanner kb2 = new Scanner(System.in);
        System.out.println("Continue? (y/n):");
        Continue = kb2.next().charAt(0);
        if(Continue=='y'){
            playcon =true;
        }
        else{
            playcon = false;
        }
        }
    }
    public void resetround(){
        pround=1;
    }
    public void  resetboard(){
        for(int row = 0; row <3; row++){
            for(int col = 0; col<3; col++){
                gboard[row][col] ='-';
            }
        }
    }
    public void printBoard(){
        for(int row = 0; row <3;row++){
            for(int col =0;col<3;col++){
                System.out.print(gboard[row][col]+" ");
                
            }
            System.out.println(" ");
        }
    }
    public void playerplay(){
        Scanner kb = new Scanner(System.in);
        int row,col;
        do{
            System.out.print("Player " + player +" input row[1-3] col[1-3]:");
            row = kb.nextInt()-1;
            col = kb.nextInt()-1; 

        }
        while(!Validmove(row,col));
        gboard[row][col] = player;
    }
    public boolean Validmove(int row,int col){
        if (row < 0 || row >= 3 || col < 0 || col >= 3 || gboard[row][col] !='-'){
            System.out.println("Invalid move, Please Move again");
            return false;
        }
        return true;
    }
    public void gamestatus(){
        if(checkwinner()){
            endgame = true;
            System.out.println("Player "+ player+" Wins!!");
        }
        if(checkboard()){
            endgame = true;
            System.out.println("Draws!!");
        }
    }
    public boolean checkwinner(){
        for(int row =0;row<3;row++){
            if(gboard[row][0] == player && gboard[row][1] == player && gboard[row][2] == player){
                return true;
            }
        }
        for(int col =0;col<3;col++){
            if(gboard[0][col] == player && gboard[1][col] == player && gboard[2][col] == player){
                return true;
            }
        }
        if(gboard[0][0] == player && gboard[1][1] == player && gboard[2][2] == player){
            return true;
        }
        if(gboard[2][0] == player && gboard[1][1] == player && gboard[0][2] == player){
            return true;
        }
        return false;
    }
    public boolean checkboard(){
        for(int row = 0; row <3;row++){
            for(int col =0; col<3;col++){
                if(gboard[row][col]== '-'){
                    return false;
                }
            }
        }
        return true;
    }
    public void switchplay(){
        if(player =='X'){
            player = 'O';
            pround++;
        }else{
            player='X';
            pround++;
        }
        
    }
    
    
    public static void main(String[] args) {
        LABXo OX = new LABXo();
       OX.playOX();
    }
}
